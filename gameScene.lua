local composer = require 'composer'
local boards = require 'board'
local gameManager = require 'gameManager'
 
local scene = composer.newScene()
local block
local board

local scoreUI
local scoreText = ' points'
local lastTimer
local timerUI
local timerText = ' seconds left'

local coverScene
local gameOverText
local finalScore

local activeTimers = {}
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 local function updateUI()
     scoreUI.text = tostring( gameManager:getScore() ) .. scoreText
 end
 
 local function updateTimer()

    local actualTimer = system.getTimer()
    local timeElapsed = actualTimer - lastTimer

    timeElapsed = 0.001 * timeElapsed -- trasformo in secondi
    gameManager:decreaseTime( timeElapsed )
    timerUI.text = tostring ( gameManager:getTimeLeft() ) .. timerText  

    lastTimer = actualTimer

 end

 local function showEndgameUI( delay )

    transition.to( coverScene, { time = delay, alpha = 1 })
    transition.to( gameOverText, { time = delay, alpha = 1 } )

    finalScore.text = 'You made ' .. gameManager:getScore() .. ' points'
    transition.to( finalScore, { time = delay, alpha = 1 } )
 end

 function scene:endgame()
    for i = 1, #activeTimers do
        timer.cancel( activeTimers[i] )
    end

    board:removeListeners()
    showEndgameUI( 2 * 1000 )

 end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view

    local backGroup = display.newGroup()
    sceneGroup:insert( backGroup )

    local gameplayGroup = display.newGroup()
    sceneGroup:insert( gameplayGroup )

    local uiGroup = display.newGroup()
    sceneGroup:insert( uiGroup )
    -- Code here runs when the scene is first created but has not yet appeared on screen
    
    gameManager = gameManager:new( self, 120 ) -- fai attenzione, è Singleton! così non potrai più creare nuovi oggetti GameManager
    board = boards:new( gameplayGroup, backGroup, 8, gameManager )

    scoreUI = display.newText( uiGroup, '', display.contentCenterX, 0.1 * display.contentHeight, native.systemFontBold, 20 )
    timerUI = display.newText( uiGroup, '', display.contentCenterX, 0.9 * display.contentHeight, native.systemFontBold, 20 )
    timerUI.text = tostring ( gameManager:getTimeLeft() ) .. timerText 

    coverScene = display.newRect( uiGroup, display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight )
    coverScene:setFillColor( 0, 0, 0 )
    coverScene.alpha = 0

    gameOverText = display.newText( uiGroup, 'GAME OVER', display.contentCenterX, 0.8 * display.contentCenterY, native.systemFontBold, 40 )
    finalScore = display.newText( uiGroup, '', display.contentCenterX, display.contentCenterY, native.systemFontBold, 20 )

    gameOverText.alpha = 0
    finalScore.alpha = 0


end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

        lastTimer = system.getTimer()
        board:setListeners()
        activeTimers[ #activeTimers + 1 ] =  timer.performWithDelay(500, updateUI, 0)
        activeTimers[ #activeTimers + 1 ] =  timer.performWithDelay( 1000, updateTimer , 0 )


 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene