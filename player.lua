local Player = {}

function Player:new()
	local instance = {}
	setmetatable( instance, self )
	self.__index = self

	instance.score = 0

	function instance:getScore()
		return self.score
	end

	function instance:setScore( x )
		local increment = ( x - 1 ) * 80 + ( 0.2 * ( x - 2 ) ) ^ 2
		self.score = self.score + increment
		self.score = math.round( self.score )
	end

	return instance
end

return Player