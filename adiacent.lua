local AdiacentBlockStrategy = {}

function mergePartialSolution( solution, partialSolution )

	for k=1,#partialSolution do
		solution[ #solution + 1 ] = partialSolution[k]
	end

	return solution
end


function AdiacentBlockStrategy:new( board )
	local instance = {}
	setmetatable( instance, self )
	self.__index = self

	instance.board = board

	function instance:removeOutOfBoundsElements( candidateMatches )
		local numElem = #candidateMatches
		local index = 1
		local k = 1
		while k <= numElem do
			local dim = self.board.dimension
			local match = candidateMatches[index]

			if match.i > dim or match.j > dim or match.i < 1 or match.j < 1 then
				table.remove( candidateMatches, index )
			else
				index = index + 1
			end

			k = k + 1 
		end
	end

	function instance:hasSameColorNeighbour( i, j, color )

		local candidateMatches = 
		{
			{ i = i+1, j = j}, 
			{ i = i-1, j=j}, 
			{ i = i, j = j-1},
			{ i = i, j = j+1},
		}

		self:removeOutOfBoundsElements( candidateMatches )

		for k = 1, #candidateMatches do
			local match = candidateMatches[k]

			if self.board.blocks[match.i][match.j]:getColor() == color then
				return true
			end
			
		end 

		return false
	end

	function instance:isPossibleNewMatch()

		local blocks = self.board.blocks
		local elements = self.board.dimension

		for i=1,elements do
			for j = 1,elements do
				local color = blocks[i][j]:getColor()
				if( self:hasSameColorNeighbour( i, j, color ) ) then
					return true -- è possible effettuare almeno un match
				end
			end
		end

		return false
	end

	function instance:replaceBlocks( blocks )
		for k = 1,#blocks do
			local position = blocks[k]
			self.board:createNewBlock( position )
		end

		local isPossible = self:isPossibleNewMatch()
		self.board:isPossibleNewMatch( isPossible )
	end

	function instance:getSameColorNeighbourgh( i,j, color, visited)
		-- caso base, 
			--> esci se nessuno dei vicini ha lo stesso colore

		-- passo iterativo
			--> dati i matches della chiamata più piccola dello stesso metodo aggiungi i nuovi trovati

		-- nota che bisogna inserire i nodi già visitiati



		local matches = {}
		local partialSolution = {}

		local candidateMatches = 
		{
			{ i = i+1, j = j}, 
			{ i = i-1, j=j}, 
			{ i = i, j = j-1},
			{ i = i, j = j+1},
		}

		-- togli i valori fuori matrice
		self:removeOutOfBoundsElements( candidateMatches )


		for k = 1, #candidateMatches do
			local match = candidateMatches[k]

			isVisited = false-- se match è già dentro visited allora esci da questa iterazione

			for k = 1, #visited do
				if visited[k].i == match.i and visited[k].j == match.j then
					isVisited = true
				end
			end

			if not isVisited then
				table.insert ( visited, { i = match.i, j = match.j } )


				if self.board.blocks[match.i][match.j]:getColor() == color then
					matches[ #matches +1 ] = match
					partialSolution = self:getSameColorNeighbourgh( match.i, match.j, color, visited )
					matches = mergePartialSolution( matches, partialSolution )
				end
			end
		end 

		return matches
	end

	function instance:searchForMatches( i, j )
		-- find every block that has near consecutive same color blocks

		local color = self.board.blocks[i][j]:getColor()


		local startBlock = {}
		startBlock.i = i
		startBlock.j = j
		
		local matches = self:getSameColorNeighbourgh( i, j, color, { startBlock } )

		if #matches > 0 then
			matches[ #matches + 1] = startBlock
		else
			matches = {} 
		end

		return matches
	end

	function instance:deleteMatches( blocks )
		-- delete every block contained in the parameter blocks
		for k=1,#blocks do
			local i = blocks[k].i
			local j = blocks[k].j 

			blocks[k].x = self.board.blocks[i][j].x
			blocks[k].y = self.board.blocks[i][j].y
			display.remove( self.board.blocks[i][j] ) 
		end
		timer.performWithDelay( 1 * 1000, function() self:replaceBlocks( blocks ) end )

		return #blocks
	end

	function instance:possibleMatchOn( indexes )
		local matches = self:searchForMatches( indexes.i, indexes.j )

		local elementsDeleted = self:deleteMatches( matches )
		return elementsDeleted
	end


	return instance
end

return AdiacentBlockStrategy