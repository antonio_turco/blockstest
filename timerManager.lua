local TimerManager = {}

function TimerManager:new( startingValueTimer, gameManager )
	
	local instance = {}

	setmetatable( instance, self )
	self._index = self

	instance.timeLeft = startingValueTimer -- in secondi
	instance.gameManager = gameManager

	function instance:getTimeLeft()
		return self.timeLeft
	end

	function instance:addTime( x )
		local increment = 10 + ( ( 0.333 * ( x-2 ) ) ^ 2 ) * 20
		self.timeLeft = self.timeLeft + increment
	end

	function instance:decreaseTime( time )


		if self.timeLeft > time then
			self.timeLeft = math.ceil( self.timeLeft - time )
		else
			self.timeLeft = 0
			self.gameManager:endgame()
		end


	end

	return instance
end

return TimerManager