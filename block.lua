local ColorType = require 'ColorType'

local Block = {}

function Block:new( gameplayGroup, dimension, board, indexes )

	dimension = dimension or 5

	local instance = {}
	setmetatable( instance, self )
	self.__index = self

	-- attributes

	local color = ColorType:getRandomColor()
	local filename = 'assets/cubes/cube_' .. color .. '.png'
	instance = display.newImageRect( gameplayGroup, filename, dimension, dimension )

	instance.color = color
	instance.dimension = dimension
	instance.board = board
	instance.horizontalIndex = indexes.horizontalIndex
	instance.verticalIndex = indexes.verticalIndex


	function instance:tap( event )
		-- tap logic
		local position = { i = self.horizontalIndex, j = self.verticalIndex }
		--print ('Send position', self.board, position.i, position.j, self.color)
		self.board:possibleMatchOn( position )
		return true
	end

	function instance:getColor()
		return self.color
	end
	
	return instance
end

return Block