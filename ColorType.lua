local ColorType = {}

-- da mettere in un file a parte???

local red = 'red'
local orange = 'orange'
local yellow = 'yellow'
local blue = 'blue'
local green = 'green'
local violet = 'violet' 

local colors = { red, orange, yellow, blue, green, violet }

function ColorType:getRandomColor()
	local color = {}
	setmetatable( color, self )
	self.__index = self

	local randomIndex = math.random( 1, #colors )
	color = colors[randomIndex]

	return color
end

return ColorType