local composer = require 'composer'

-- Seed the random number generator
math.randomseed( os.time() )
display.setStatusBar( display.HiddenStatusBar )

composer.gotoScene('gameScene')