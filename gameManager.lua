local player = require 'player' 
local timerManager = require 'timerManager'

local gameManager = {}

function gameManager:new( gameplayScene, startingValueTimer )
	
	local instance = {}

	setmetatable( instance, self )
	self._index = self

	instance.gameEnded = false
	instance.player = player:new()
	instance.timerManager = timerManager:new( startingValueTimer, instance )
	instance.gameplayScene = gameplayScene

	function instance:updateScore( x )
		if x > 1 then
			self.player:setScore( x )
			self.timerManager:addTime( x )
		end
	end

	function instance:getScore()
		local score = self.player:getScore()
		return score
	end


	function instance:decreaseTime( time )
		self.timerManager:decreaseTime( time )
	end

	function instance:getTimeLeft()
		return self.timerManager:getTimeLeft()
	end

	function instance:endgame()
		self.gameplayScene:endgame()
	end

	return instance
end

return gameManager