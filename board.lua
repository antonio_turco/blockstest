local block = require 'block'

local strategy = require 'adiacent'

local Board = {}

function Board:new( gameplayGroup, backGroup, dimension, gameManager )
	local instance = {}
	setmetatable( instance, self )
	self.__index = self

	local boardDimension = 0.95 * display.contentWidth
	-- definisce rettangolo di gioco
	instance.background = display.newRect( backGroup, display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight )
	instance.background:setFillColor( 0.2, 0.8, 0.3 )
	instance.foreground = display.newRect( backGroup, display.contentCenterX, display.contentCenterY, 1.05 * boardDimension, 1.05 * boardDimension )
	instance.foreground:setFillColor( 0.2, 0.1, 0.1 )

	instance.dimension = dimension

	local blockDimension = 0.9 * display.contentWidth/dimension
	instance.blockDimension = blockDimension
	instance.gameplayGroup = gameplayGroup
	instance.gameManager = gameManager
	--blockDimension = blockDimension / display.contentWidth

	-- genera una matrice dimension x dimension
	-- con all'interno i vari blocchi di grandezza opportuna
	instance.blocks = {}
	instance.strategy = strategy:new( instance )

	local yBlockPosition = 0.875 * display.contentHeight - boardDimension

	for i=1, instance.dimension do
		instance.blocks[i] = {}
		local xBlockPosition = 0.1 * display.contentWidth

		for j = 1, instance.dimension do
			instance.blocks[i][j] = block:new( gameplayGroup, blockDimension, instance, { horizontalIndex = i, verticalIndex = j } )
			instance.blocks[i][j].x = xBlockPosition
			instance.blocks[i][j].y = yBlockPosition
			xBlockPosition = xBlockPosition + blockDimension
		end

		yBlockPosition = yBlockPosition + blockDimension
	end


	function instance:setListeners()
		for i = 1, self.dimension do
			for j = 1, self.dimension do
				self.blocks[i][j]:addEventListener( 'tap', self.blocks[i][j] )
			end
		end
	end

	function instance:removeListeners()
		for i = 1, self.dimension do
			for j = 1, self.dimension do
				self.blocks[i][j]:removeEventListener( 'tap', self.blocks[i][j] )
			end
		end
	end

	function instance:createNewBlock( position )
		self.blocks[position.i][position.j] = block:new( self.gameplayGroup, self.blockDimension, self, { horizontalIndex = position.i, verticalIndex = position.j } )
		self.blocks[position.i][position.j].x = position.x
		self.blocks[position.i][position.j].y = position.y
		self.blocks[position.i][position.j]:addEventListener('tap', self.blocks[position.i][position.j])
	end

	function instance:possibleMatchOn( indexes )
		local deletedElements = self.strategy:possibleMatchOn( indexes )
		self.gameManager:updateScore( deletedElements )
	end

	function instance:isPossibleNewMatch( isPossible )
		if not isPossible then
			self.gameManager:endgame()
		end
	end
	
	return instance
end

return Board